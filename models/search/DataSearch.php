<?php

namespace app\models\search;

use yii\base\Model;
use app\components\XmlDataInArray;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;


class DataSearch extends Model
{
    public $id;
    public $price;
    public $categoryId;
    public $hidden;

    private $_filtered = false;


    public function rules()
    {
        return [
            [['id', "price",'categoryId', 'hidden'], 'integer']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
     public function search(array $params)
     {  
        if ($this->load($params) && $this->validate()) {
            $this->_filtered = true;
        }

        $provider = new ArrayDataProvider([
        'allModels' => $this->getData(),
        'sort' => [
        'attributes' => ['id', 'categoryId', 'hidden', 'price'],
         ],
          ]);

        return $provider;
    }


     protected function getData()
    {
         $xml_products = new XmlDataInArray(["filename" => \Yii::getAlias('@app/xml/products.xml')]);
         $data = $xml_products->dataArray;
           if ($this->_filtered) {
            $data = array_filter($data, function ($value) {
                $conditions = [true];
                if (!empty($this->id)) {
                    $conditions[] = strpos($value['id'], $this->id) !== false;
                }
                if (!empty($this->categoryId)) {
                    $conditions[] = strpos($value['categoryId'], $this->categoryId) !== false;
                }

                if (!empty($this->price)) {
                    $conditions[] = strpos($value['price'], $this->price) !== false;
                }
                if (isset($this->hidden) &&  $this->hidden !== '') {
                    $conditions[] = strpos($value['hidden'], $this->hidden) !== false;
                }
                return array_product($conditions);
            });
        }

        return $data;
    }

    public function categoryList() {
        $xml_cat = new XmlDataInArray(["filename" => \Yii::getAlias('@app/xml/categories.xml')]);
        return ArrayHelper::map($xml_cat->dataArray, "id", "name");
    }

      public function hiddenList() {
        
        return ["No", "Yes"];
    }

}
