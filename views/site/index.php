<?php

/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$this->title = 'My Yii Application';?>
 
 <h3>Products</h3>
 
 <?= GridView::widget([
    'dataProvider' => $dataProvider,
     'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '[нет данных]'],
    'filterModel' => $filter,
     'columns' => [
        'id',
        ['attribute' =>"categoryId",
          //'label' => "Category",
          'filter' => $filter->categoryList(),
          'value' => function($model) use($filter) {
          	return ArrayHelper::getValue($filter->categoryList(), $model["categoryId"]);
          }
        ],
        'price',
         ['attribute' =>"hidden",
          'filter' => $filter->hiddenList(),
          'value' => function($model) use($filter) {
          	return ArrayHelper::getValue($filter->hiddenList(), $model["hidden"]);
          }
        ],
    ],
]);

?>
<h4>Форматирование данных (номера телефона)</h4>
<? echo Yii::$app->formatter->asPhone('375292724043', "BY")?> </br>
<? echo Yii::$app->formatter->asPhone('79062544545')?> </br>
<? echo Yii::$app->formatter->asPhone('123456789333')?>
<h4>Хелперы</h4>
 yiiframework.laravel.symphony - <?= \yii\helpers\StringHelper::byteSubstr ( "yiiframework.laravel.symphony", 0, 12) ?> </br>
  created_at - <?= \yii\helpers\Inflector::camelize('created_at') ?> </br>

 'Купи слона' - <?= \yii\helpers\Inflector::slug('Купи слона',' ', false);?> </br>
<div class="site-index">
    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
