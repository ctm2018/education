<?php
namespace app\components;

use yii\i18n\Formatter;

class FormatterHelper extends Formatter {

  public function asPhone($phone, $region = null) {
    $cleaned = preg_replace('/[^[:digit:]]/', '', $phone);
      if (strlen($phone) == 12 && $region == "BY" ) {
        preg_match('/(\d{3})(\d{2})(\d{3})(\d{2})(\d{2})/', $cleaned, $matches);
        return "+{$matches[1]} ({$matches[2]}) {$matches[3]}-{$matches[4]}-{$matches[5]}";
      } 
      elseif (strlen($phone) == 12) {
        preg_match('/(\d{2})(\d{3})(\d{3})(\d{2})(\d{2})/', $cleaned, $matches);
        return "+{$matches[1]} ({$matches[2]}) {$matches[3]}-{$matches[4]}-{$matches[5]}";
      } 
      elseif (strlen($phone) == 11) {
        preg_match('/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/', $cleaned, $matches);
        return "+{$matches[1]} ({$matches[2]}) {$matches[3]}-{$matches[4]}-{$matches[5]}";
      }
      else {
        return $cleaned;
      }
    }
}
