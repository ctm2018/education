<?php
namespace app\components;

use yii\base\BaseObject;
use yii\helpers\Json;

class XmlDataInArray extends BaseObject
{
    /**
     * @var string имя XML-файла для чтения
     */
    public $filename;
     /**
     * @var string key in xml
     */
    public $keyitem = "item";
    /**
     * @var array
     */
    private $array;

    public function init()
    {
        parent::init();
       
        $xml= simplexml_load_file($this->filename);
        $json = Json::encode($xml);
        $array =Json::decode($json,TRUE);
        $this->array = $array[$this->keyitem];

    }

      public function getDataArray()
    {
      return  $this->array;
    }
}
